CarCar

Team:

Mitchell Mora - Service
Rafael Guardon - Sales
Getting Started

Make sure you have Docker, Git and Node.js 18.2 or above

First, please fork this repository.
Second, please clone the forked repository onto your local computer: git clone <<repository.url.here>>
Third, please build and run the project using Docker with the following commands:
    docker volume create beta-data
    docker-compose build
    docker-compose up
After running these commands, make sure all of the Docker containers are running

View the project in the browser at http://localhost:3000/

Design
![Alt text](<CARCAR image.png>) (CTRL + click to open)

CarCar is made up of 3 microservices which are dependent on one and other:

(INSERT EXACLIDRAW)

Integration - How our microservices work together to maintain the application

Our Sales and Service domains work together with our Inventory domain to make everything in the CarCar application work seamlessly.

In our application, the inventory domain acts as the root domain for keeping track of the automobiles that are on our lot and are available to purchase. Our sales and service microservices obtain information from the inventory domain through polling, which is a process within which the inventory domain will send data about the automobile inventory to each of the sales and services microservices. Both of these microservices are then able to regularly update their own internal database with the latest data from the inventory application.

## Inventory

## Manufacturers:
From Insomnia and your browser, you can access the manufacturer endpoints at the following URLs.

Action	Method	URL
List manufacturers	GET	http://localhost:8100/api/manufacturers/
Create a manufacturer	POST	http://localhost:8100/api/manufacturers/
Get a specific manufacturer	GET	http://localhost:8100/api/manufacturers/id/
Update a specific manufacturer	PUT	http://localhost:8100/api/manufacturers/id/
Delete a specific manufacturer	DELETE	http://localhost:8100/api/manufacturers/id/

## Creating and updating a manufacturer requires only the manufacturer's name.

json
{
  "name": "Mercedes"
}
The return value of creating, getting, and updating a single manufacturer is its name, href, and id.

json
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Mercedes"
}

## The list of manufacturers is a dictionary with the key "manufacturers" set to a list of manufacturers.

json

{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Benz-Mercedes"
    }
  ]
}

## Vehicle Models:
From Insomnia and your browser, you can access the manufacturer endpoints at the following URLs.

Action	Method	URL
List vehicle models	GET	http://localhost:8100/api/models/
Create a vehicle model	POST	http://localhost:8100/api/models/
Get a specific vehicle model	GET	http://localhost:8100/api/models/id/
Update a specific vehicle model	PUT	http://localhost:8100/api/models/id/
Delete a specific vehicle model	DELETE	http://localhost:8100/api/models/id/
Creating a vehicle model requires the model name, a URL of an image, and the id of the manufacturer.



{
  "name": "",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg/1200px-Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg",
  "manufacturer_id": 1
}

## Updating a vehicle model can take the name and/or the picture URL. It is not possible to update a vehicle model's manufacturer.



{
  "name": "W205",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg/1200px-Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg"
}

## Getting the detail of a vehicle model, or the return value from creating or updating a vehicle model, returns the model's information and the manufacturer's information.

json

{
  "href": "/api/models/1/",
  "id": 1,
  "name": "W205",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "MERCEDES"
  }
}

## Getting a list of vehicle models returns a list of the detail information with the key "models".

{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "W205",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg/1200px-Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Mercedes"
      }
    }
  ]
}

## Automobiles
From Insomnia and your browser, you can access the automobile endpoints at the following URLs.

Note: The identifiers for automobiles in this API are not integer ids. They are the Vehicle Identification Number (VIN) for the specific automobile.

Action	Method	URL
List automobiles	GET	http://localhost:8100/api/automobiles/
Create an automobile	POST	http://localhost:8100/api/automobiles/
Get a specific automobile	GET	http://localhost:8100/api/
From Insomnia and your browser, you can access the automobile endpoints at the following URLs.

Note: The identifiers for automobiles in this API are not integer ids. They are the Vehicle Identification Number (VIN) for the specific automobile.

Action	Method	URL
List automobiles	GET	http://localhost:8100/api/automobiles/
Create an automobile	POST	http://localhost:8100/api/automobiles/
Get a specific automobile	GET	http://localhost:8100/api/automobiles/vin/
Update a specific automobile	PUT	http://localhost:8100/api/automobiles/vin/
Delete a specific automobile	DELETE	http://localhost:8100/api/automobiles/vin/

## You can create an automobile with its color, year, VIN, and the id of the vehicle model.

{
  "color": "Yellow",
  "year": 1995,
  "vin": "1X2345XYZ123555",
  "model_id": 1
}
Note: Please make sure VIN is no longer than 17 characters, the standard length of a VIN. As noted, you query an automobile by its VIN. For example, you would use the URL:

http://localhost:8100/api/automobiles/1X2345XYZ123555/

## Get the details for the automobile with the VIN "1X2345XYZ123555". The details for an automobile include its model and manufacturer.



{
  "href": "/api/automobiles/1X2345XYZ123555/",
  "id": 1,
  "color": "yellow",
  "year": 1995,
  "vin": "1X2345XYZ123555",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Highlander",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg/1200px-Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Mercedes"
    }
  },
  "sold": false
}

## Update the color, year, and sold status of an automobile.

json

{
  "color": "red",
  "year": 2012,
  "sold": true
}

## Get a list of automobiles returns a dictionary with the key "autos" set to a list of automobile information.
{
  "autos": [
    {
      "href": "/api/automobiles/1X2345XYZ123555/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1X2345XYZ123555",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Mercedes",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg/1200px-Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Mercedes"
        }
      },
      "sold": false
    }
  ]
}

## Sales

In the sales microservice, there are four models: AutomobileVO, Customer, Salesperson, and Sale. The first three mentioned all send data to the view that creates a new sale to create a new Sale object.

The AutomobileVO is a value object and is constantly being updated by the poller, which is polling the inventory API database and has up-to-date data on the automobile objects in the inventory API, down to the second.

Accessing Endpoints to Send and View Data through Insomnia:
Customers:
Action	Method	URL
List customers	GET	http://localhost:8090/api/customers/
Create a customer	POST	http://localhost:8090/api/customers/
Delete a customer	DELETE	http://localhost:8090/api/customers/id/delete/

## To create a Customer, send this JSON Body:

json
{
  "name": "Test Person",
  "address": "123 Test Way",
  "phone_number": 123 4911
}
Return value of a created customer:

json

{
  "id": 1,
  "name": "Test Person",
  "address": "123 Test Way",
  "phone_number": 123 4911
}

## List Customers:

json

{
  "customers": [
    {
      "id": 1,
      "name": "Test Person",
      "address": "123 Test Way",
      "phone_number": 123 4911
    },
    {
      "id": 2,
      "name": "Rafael",
      "address": "1700 Test Dr",
      "phone_number": "00000000"
    }
  ]
}

## Salespeople:

Action	Method	URL
List salespeople	GET	http://localhost:8090/api/salespeople/
Create a salesperson	POST	http://localhost:8090/api/salespeople/
Delete a salesperson	DELETE	http://localhost:8090/api/salesperson/id/delete/

## To create a salesperson, send this JSON body:

json

{
  "name": "Rafael McSales",
  "employee_id": "23"
}
Return value of creating a salesperson:

json

{
  "id": 1,
  "name": "Rafael McSales",
  "employee_id": "23"
}

## List all salespeople return value:

json

{
  "salespeople": [
    {
      "id": 1,
      "name": "Rafael McSales",
      "employee_id": "23"
    }
  ]
}

## Record of Sales:
List all sales records	GET	http://localhost:8090/api/sales/
Create a new sale	POST	http://localhost:8090/api/sales/
Delete a sale	DELETE	http://localhost:8090/api/sale/id/delete/

## List all sales return value:

json

{
  "sales": [
    {
      "automobile": {
        "vin": "1234567",
        "import_id": 7,
        "sold": false
      },
      "salesperson": {
        "id": 13,
        "name": "Mitch",
        "employee_number": 42
      },
      "customer": {
        "name": "Doja Cat Jr",
        "address": "123 Front St",
        "phone_number": "7073214342"
      },
      "id": 16
    }
  ]
}

## Create a new sale:

json

{
  "price": 50000,
  "automobile": {
    "id": 12
  },
  "salesperson": {
    "id": 17
  },
  "customer": {
    "id": 10
  }
}
Return value of creating a new sale:

json

{
  "price": 50000,
  "vin": {
    "vin": "6666666"
  },
  "salesperson": {
    "id": 12,
    "name": "Shelly Anne",
    "employee_number": 19
  },
  "customer": {
    "id": 4,
    "name": "Bob ETC",
    "address": "3333 A street",
    "phone_number": "5102220055"
  },
  "id": 20
}
## AutomobileVO
This is a value object based on the automobiles in the inventory microservice. They are created via a poller, which polls the inventory microservice database every 60 seconds, checks for changes to the automobile objects, and updates the sales microservice database accordingly.

## Service Microservice
The service microservice handles the side of the dealership relating to vehicle maintenance and repair. There are three models in this microservice: AutomobileVO, Technician, and Appointment.

## Technicians
## Endpoints

Action	Method	URL
List technicians	GET	http://localhost:8080/api/technicians/
Create a technician	POST	http://localhost:8080/api/technicians/
Delete a technician	DELETE	http://localhost:8080/api/technicians/id/
List technicians: This endpoint returns a list of all currently employed technicians. The method must be "get," and no data needs to be provided. See the example below of output:

json

{
  "technicians": [
    {
      "id": 1,
      "first_name": "Shakira",
      "last_name": "Shakira's Last Name",
      "employee_id": 23
    }
  ]
}
## Create a technician: Use this endpoint to create a new technician. The request method must be "post," and data must be provided as follows:

json

{
  "first_name": "Shakira",
  "last_name": "Shakira's last name",
  "employee_id": 23
}
Once the request is submitted, upon successful creation, the details of the new technician will be returned in the following format:

json

{
  "id": 5,
  "first_name": "Shakira",
  "last_name": "Shakira's last name",
  "employee_id": 23
}
Note: Every technician has an id, which is unique and automatically generated by the system when the technician instance is created, and an employee id, which is chosen by the user and manually assigned during the creation process.

## Delete a technician: Use this endpoint to delete a specific technician. The id represents the technician's unique id (NOT the manually-assigned employee ID). The id should, therefore, be replaced with the id of the technician to be deleted. For example: http://localhost:8080/api/technicians/2/ . To use this endpoint, the request method must be "delete," and no data needs to be provided. Upon successful deletion, the following response will be returned:

json

{
  "message": "technician 23 deleted"
}
Note: The number in this message is the employee ID number that was manually assigned to that technician upon creation.

## Appointments
## Endpoints

Action	Method	URL
List appointments	GET	http://localhost:8080/api/appointments/
Create an appointment	POST	http://localhost:8080/api/appointments/
Cancel an appointment	PUT	http://localhost:8080/api/appointments/id/cancel/
Finish an appointment	PUT	http://localhost:8080/api/appointments/id/finish/
List appointments: This endpoint returns a list of all currently and previously scheduled appointments, including those that have a status of canceled. The request method must be "get," and no data needs to be provided. See the example below of output:

json
{
  "appointments": [
    {
      "id": 7,
      "date_time": "2023-07-28T12:00:00+00:00",
      "reason": "raccoon",
      "status": "scheduled",
      "vin": "123XXX543",
      "customer": "",
      "technician": {
        "id": 7,
        "first_name": "Bad",
        "last_name": "Gyal",
        "employee_id": 8
      }
    }
  ]
}

## Create an appointment: Use this endpoint to create a new appointment. The request method must be "post," and data must be provided as follows:

json

{
  "date_time": "2023-11-13T08:14:00",
  "reason": "test final",
  "vin": "555CCCVVV",
  "customer": "Shakira",
  "technician": 1
}
IMPORTANT: Please make sure that the VIN has no more than 17 characters, as this is the standard length of a VIN. The system will accept VINs with fewer than 17 characters but not more.

Note: The id that is submitted in the technician field must correspond to an active technician in the system; otherwise, an error will be thrown.

Once the request is submitted, upon successful creation, the details of the new appointment will be returned in the following format:

json
{
  "id": 6,
  "date_time": "2023-11-13T08:14:00",
  "reason": "test final",
  "status": "scheduled",
  "vin": "555CCCVVV",
  "customer": "Shakira",
  "technician": {
    "id": 1,
    "first_name": "Bad",
    "last_name": "Gyal",
    "employee_id": 8
  }

## Cancel an appointment: Use this endpoint to cancel an existing appointment. The id represents the appointment's unique id. The id should, therefore, be replaced with the id of the appointment to be canceled. For example: http://localhost:8080/api/appointments/5/cancel The request method must be "put," and no data needs to be provided. Once the request is submitted, if the appointment exists in the database, the status property of the referenced appointment instance is changed to "canceled," and the following message is returned:

json
{
  "message": "status updated to canceled"
}

## Finish an appointment: Use this endpoint to update an existing appointment's status once it is completed. The id represents the appointment's unique id. The id should, therefore, be replaced with the id of the appointment to be updated. For example: http://localhost:8080/api/appointments/5/finish The request method must be "put," and no data needs to be provided. Once the request is submitted, if the appointment exists in the database, the status property of the referenced appointment instance is changed to "finished," and the following message is returned:

json

{
  "message": "status updated to finished"
}


## AutomobileVO
This is a value object based on the automobiles in the inventory microservice. They are created via a poller, which polls the inventory microservice database every 60 seconds, checks for changes to the automobile objects, and updates the service microservice database accordingly.
