import React from 'react';

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '500px', height: '500px', border: '1px solid #ccc' }}>
          <img src="https://images.squarespace-cdn.com/content/v1/51cdafc4e4b09eb676a64e68/1470951916671-6BFFPWQEG68L0R9LZL28/McQueen12.jpg" alt="" style={{ width: '100%', height: '100%' }} />
        </div>
      </div>
    </div>
  );
}

export default MainPage;
